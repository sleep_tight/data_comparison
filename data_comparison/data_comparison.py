from typing import Optional

import pandas as pd
from pandas import DataFrame


class DataComparison:
    """Data comparison class."""

    @staticmethod
    def compare(actual_data: DataFrame, expected_data: DataFrame,
                columns: Optional[list] = None) -> 'ComparisonResult':
        """
        Compare expected data with actual.
        :param actual_data: DataFrame with actual data
        :param expected_data: DataFrame with expected data
        :param columns: columns to compare
        :return: comparison result
        """
        if not columns:
            columns = list(actual_data.columns)

        result = pd.merge(actual_data, expected_data[columns], on=columns,
                          how='outer', suffixes=['', '_'], indicator=True)
        status = actual_data[columns].equals(expected_data[columns])

        return ComparisonResult(status, result)

    @staticmethod
    def contains(actual_data: DataFrame, expected_data: DataFrame,
                 columns: Optional[list] = None) -> 'ComparisonResult':
        """
        Check if expected data is in actual.
        :param actual_data: DataFrame with actual data
        :param expected_data: DataFrame with expected data
        :param columns: columns to check
        :return: check result
        """
        if not columns:
            columns = list(actual_data.columns)

        result = pd.merge(actual_data[columns], expected_data, on=columns,
                          how='right', suffixes=['', '_'], indicator=True)
        extra_rows = result.loc[result['_merge'] == 'right_only']
        status = extra_rows.empty

        return ComparisonResult(status, result)


class ComparisonResult:
    """Comparison result class."""

    def __init__(self, status: bool, result: DataFrame) -> None:
        """
        Initialize
        :param status: comparison status
        :param result: comparison result data
        """
        self.status = status
        self.result = result

        merge_status_map = {
            'both': 'equal',
            'left_only': 'missed_row',
            'right_only': 'extra_row'
        }

        self.result.rename(columns={'_merge': 'comparison_result'},
                           inplace=True)
        self.result['comparison_result'] = self.result['comparison_result']\
            .map(merge_status_map)

    @property
    def equal_rows(self) -> DataFrame:
        """Get equal rows."""
        return self.result.loc[self.result['comparison_result'] == 'equal']

    @property
    def missed_rows(self) -> DataFrame:
        """Get missed in expected data rows."""
        return self.result.loc[self.result['comparison_result'] == 'missed_row']

    @property
    def extra_rows(self) -> DataFrame:
        """Get missed in actual data rows but presented in expected."""
        return self.result.loc[self.result['comparison_result'] == 'extra_row']

    def report(self) -> str:
        """Generate human readable report."""
        report = f"Status: {self.status}\n\n"
        if not self.equal_rows.empty:
            report += f"Equal rows: \n{self.equal_rows.to_string()}\n\n"
        if not self.missed_rows.empty:
            report += f"Missed rows: \n{self.missed_rows.to_string()}\n\n"
        if not self.extra_rows.empty:
            report += f"Extra rows: \n{self.extra_rows.to_string()}\n\n"
        return report

    def export_to_csv(self) -> str:
        """Generate csv with report."""
        return self.result.to_csv(sep=';')
