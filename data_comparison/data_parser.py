import json
from abc import ABC, abstractmethod
from pathlib import Path
from typing import Union

import pandas as pd
import requests
from pandas import DataFrame


class BaseDataParser(ABC):
    """Base data parser class"""

    def parse_api_data(self, url: str) -> DataFrame:
        """
        Parse data from REST API.
        :param url: link to data api
        :return: parsed dataframe
        """
        response = requests.get(url)
        if not response:
            raise Exception(
                f"No data found: {response.status_code} {response.reason}")

        return self.read_json(response.json())

    def parse_file_data(self, file: Path,
                        file_format: str = 'csv') -> DataFrame:
        """
        Parse data from file.
        :param file: file with expected data
        :param file_format: file format, currently supported [csv, json]
        :return: parsed dataframe
        """
        if file_format == 'csv':
            return self.read_csv(file)
        if file_format == 'json':
            with open(file) as json_file:
                data = json.load(json_file)
            return self.read_json(data)
        else:
            raise ValueError(f"File type {file_format} is not supported")

    @staticmethod
    @abstractmethod
    def read_json(json_data: Union[list, dict]) -> DataFrame:
        """
        Read json data.
        :param json_data: original json with data
        :return: parsed data
        """
        raise NotImplementedError

    @staticmethod
    @abstractmethod
    def read_csv(csv: Union[Path, str]) -> DataFrame:
        """
        Read csv data.
        :param csv: csv with data
        :return: parsed data
        """
        raise NotImplementedError


class OpendatasoftParser(BaseDataParser):
    """Data comparison class for opendatasoft formatted data."""

    @staticmethod
    def read_json(json_data: Union[list, dict]) -> DataFrame:
        """
        Read json data.
        :param json_data: original json with data
        :return: parsed data
        """
        records = json_data.get('records') if \
            isinstance(json_data, dict) else json_data
        data = [record.get('fields') for record in records]
        return pd.DataFrame(data)

    @staticmethod
    def read_csv(csv: Union[Path, str]) -> DataFrame:
        """
        Read csv data.
        :param csv: csv with data
        :return: parsed data
        """
        data = pd.read_csv(csv, sep=';')
        data.columns = data.columns.str.lower()
        return data


class DataParser(ABC):
    """
    Factory class which returns data parser class depending on data source.
    """

    @staticmethod
    def source(data_source: str) -> BaseDataParser:
        """
        Create class depending on source.
        :param data_source: data source
        :return: data parser
        """
        if data_source == 'opendatasoft':
            return OpendatasoftParser()
        else:
            raise ValueError(f"Source {data_source} is not supported")
