import pytest
from pandas import DataFrame

from data_comparison import BaseDataParser, DataParser


@pytest.fixture
def parser() -> BaseDataParser:
    return DataParser.source('opendatasoft')


class TestDataParser:
    """Test DataParser class"""

    CORRECT_CSV = """PassengerId;Survived;Pclass;Name;Sex;Age;SibSp;Parch;Ticket;Fare;Cabin;Embarked
152;Yes;1;Pears, Mrs. Thomas (Edith Wearne);female;22.0;1;0;113776;66.6;C2;S
479;No;3;Karlsson, Mr. Nils August;male;22.0;0;0;350060;7.5208;;S"""

    CORRECT_JSON = """
[
    {
        "datasetid": "titanic-passengers",
        "recordid": "53ed23eba68e5afaf535229f674a920125038aba",
        "fields": {
            "name": "Pears, Mrs. Thomas (Edith Wearne)",
            "age": 22.0,
            "passengerid": 152
        },
        "record_timestamp": "2016-09-21T00:34:51.313+02:00"
    },
    {
        "datasetid": "titanic-passengers",
        "recordid": "2a608c3197faa6a4a18730ead24b2753ea7720fb",
        "fields": {
            "name": "Karlsson, Mr. Nils August",
            "age": 22.0,
            "passengerid": 479
        },
        "record_timestamp": "2016-09-21T00:34:51.313+02:00"
    }
]
"""

    def test_api_parse_correct_url(self, parser):
        url = "https://public.opendatasoft.com/api/records/1.0/search/?dataset=titanic-passengers&q=age%3D22&rows=27"
        parsed_data = parser.parse_api_data(url)

        assert isinstance(parsed_data, DataFrame)
        assert 152 in parsed_data.passengerid.values

    def test_api_parse_wrong_url(self, parser):
        url = "https://public.opendatasoft.com/api/records/1.0/search/?dataset=titanic-passenge"
        with pytest.raises(Exception) as exc:
            parser.parse_api_data(url)

        assert "No data found" in str(exc.value)

    def test_file_parse_correct_csv(self, parser, tmp_path):
        file = tmp_path / "test.csv"
        file.write_text(self.CORRECT_CSV)
        parsed_data = parser.parse_file_data(file)

        assert isinstance(parsed_data, DataFrame)
        assert 152 in parsed_data.passengerid.values

    def test_file_parse_correct_json_file(self, parser, tmp_path):
        file = tmp_path / "test.json"
        file.write_text(self.CORRECT_JSON)
        parsed_data = parser.parse_file_data(file, 'json')

        assert isinstance(parsed_data, DataFrame)
        assert 152 in parsed_data.passengerid.values

    def test_file_parse_not_existing_file(self, parser):
        file = "not_existing_test.txt"
        with pytest.raises(FileNotFoundError):
            parser.parse_file_data(file)

    def test_file_parse_wrong_format(self, parser):
        file_format = 'txt'
        with pytest.raises(ValueError) as exc:
            parser.parse_file_data("text.txt", file_format)

        assert "is not supported" in str(exc.value)
