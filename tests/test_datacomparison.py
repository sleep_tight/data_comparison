import pandas as pd

from data_comparison import DataComparison as datacom


class TestDataComparison:
    """Test DataParser class"""

    ACTUAL_DATA = [
        {'passengerid': 152, 'name': 'Pears, Mrs. Thomas (Edith Wearne)', 'age': 22},
        {'passengerid': 479, 'name': 'Karlsson, Mr. Nils August', 'age': 22},
        {'passengerid': 709, 'name': 'Cleaver, Miss. Alice', 'age': 22},
    ]

    EXPECTED_DATA_INCOMPLETE = [
        {'passengerid': 152, 'name': 'Pears, Mrs. Thomas (Edith Wearne)', 'age': 22},
        {'passengerid': 479, 'name': 'Karlsson, Mr. Nils August', 'age': 22},
    ]

    EXPECTED_DATA_CHANGED = [
        {'passengerid': 152, 'name': 'Pears, Mrs. Thomas (Edith Wearne)', 'age': 22},
        {'passengerid': 479, 'name': 'Karlsson, Mr. Nils August', 'age': 22},
        {'passengerid': 709, 'name': 'Anonymous', 'age': 22},
    ]

    def test_compare_equal_data(self):
        actual_df = pd.DataFrame(self.ACTUAL_DATA)
        expected_df = pd.DataFrame(self.ACTUAL_DATA)

        result = datacom.compare(actual_df, expected_df)

        assert result.status
        assert result.missed_rows.empty
        assert result.extra_rows.empty

    def test_compare_incomplete_expected_data(self):
        actual_df = pd.DataFrame(self.ACTUAL_DATA)
        expected_df = pd.DataFrame(self.EXPECTED_DATA_INCOMPLETE)

        result = datacom.compare(actual_df, expected_df)

        assert not result.status
        assert len(result.missed_rows.index) == 1
        assert result.extra_rows.empty

    def test_compare_changed_expected_data(self):
        actual_df = pd.DataFrame(self.ACTUAL_DATA)
        expected_df = pd.DataFrame(self.EXPECTED_DATA_CHANGED)

        result = datacom.compare(actual_df, expected_df)

        assert not result.status
        assert len(result.missed_rows.index) == 1
        assert len(result.extra_rows.index) == 1
        assert result.extra_rows['name'].any() == 'Anonymous'

    def test_compare_selected_columns(self):
        actual_df = pd.DataFrame(self.ACTUAL_DATA)
        expected_df = pd.DataFrame(self.EXPECTED_DATA_CHANGED)

        result = datacom.compare(actual_df, expected_df, ['passengerid', 'age'])

        assert result.status
        assert result.missed_rows.empty
        assert result.extra_rows.empty

    def test_compare_selected_columns_incomplete_expected_data(self):
        actual_df = pd.DataFrame(self.ACTUAL_DATA)
        expected_df = pd.DataFrame(self.EXPECTED_DATA_INCOMPLETE)

        result = datacom.compare(actual_df, expected_df, ['passengerid', 'age'])

        assert not result.status
        assert len(result.missed_rows.index) == 1
        assert result.extra_rows.empty

    def test_contains_equal_data(self):
        actual_df = pd.DataFrame(self.ACTUAL_DATA)
        expected_df = pd.DataFrame(self.ACTUAL_DATA)

        result = datacom.contains(actual_df, expected_df)

        assert result.status

    def test_contains_incomplete_expected_data(self):
        actual_df = pd.DataFrame(self.ACTUAL_DATA)
        expected_df = pd.DataFrame(self.EXPECTED_DATA_INCOMPLETE)

        result = datacom.contains(actual_df, expected_df)

        assert result.status
        assert result.missed_rows.empty

    def test_contains_changed_expected_data(self):
        actual_df = pd.DataFrame(self.ACTUAL_DATA)
        expected_df = pd.DataFrame(self.EXPECTED_DATA_CHANGED)

        result = datacom.contains(actual_df, expected_df)

        assert not result.status
        assert len(result.extra_rows.index) == 1
        assert result.extra_rows['name'].any() == 'Anonymous'

    def test_contains_selected_columns_changed_expected_data(self):
        actual_df = pd.DataFrame(self.ACTUAL_DATA)
        expected_df = pd.DataFrame(self.EXPECTED_DATA_INCOMPLETE)

        result = datacom.contains(actual_df, expected_df, ['passengerid', 'age'])

        assert result.status
