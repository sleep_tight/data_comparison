from setuptools import setup, find_packages

setup(
    name='data_comparison',
    version='0.2.0',
    description='Data comparison module',
    author='Nika',
    author_email='nika.butko@post.com',
    url='https://bitbucket.org/sleep_tight/data_comparison/',
    license='MIT',
    packages=find_packages(),
    install_requires=[
        'pandas',
        'requests',
    ],
    extras_require={
        'dev': ['pytest', 'wheel', 'flake8'],
    },
    python_requires='>=3.7',
)
