# Data comparison
The library allows to get data from REST API or local csv or json files, 
compare it and get comparison report. Currently supported opendatasoft format.

## Installation
* Production installation
```
pip install path/to/data_comparison
```
* Development installation
```
pip install -e path/to/data_comparison['dev']
```
* Run tests
```
pytest path/to/data_comparison
```
* Build distribution package
```
cd path/to/data_comparison
python3 setup.py sdist bdist_wheel
```

## Basic usage
Download data from API and csv file
```python
from pathlib import Path

from data_comparison import DataComparison as datacom, DataParser

dataset_url = "https://public.opendatasoft.com/api/records/1.0/search/?dataset=titanic-passengers&q=age%3D22&rows=27"
dataset_file = Path('titanic_passengers_dataset.csv')

parser = DataParser.source('opendatasoft')
actual_data = parser.parse_api_data(dataset_url)
expected_data = parser.parse_file_data(dataset_file)
```

Compare whole dataframes
```python
res = datacom.compare(actual_data, expected_data)
```
Compare selected columns from dataframes
```python
res = datacom.compare(actual_data, expected_data, ['passengerid', 'name'])
```
Check if expected data is in actual data
```python
res = datacom.compare(actual_data, expected_data)
```
Print report
```python
print(res.report())
```
Print csv report
```python
print(res.export_to_csv())
```
